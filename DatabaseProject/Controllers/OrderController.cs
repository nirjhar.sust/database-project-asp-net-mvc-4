﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatabaseProject.Controllers
{
    public class OrderController : Controller
    {
        private ScerpEntities db = new ScerpEntities();

        //
        // GET: /Order/

        public ActionResult Index()
        {
            var product_order = db.product_order.Include(p => p.manufacturer).Include(p => p.product);
            return View(product_order.ToList());
        }

        //
        // GET: /Order/Details/5

        public ActionResult Details(int id = 0)
        {
            product_order product_order = db.product_order.Find(id);
            if (product_order == null)
            {
                return HttpNotFound();
            }
            return View(product_order);
        }

        //
        // GET: /Order/Create

        public ActionResult Create()
        {
            ViewBag.id_manufacturers = new SelectList(db.manufacturers, "id_manufacturers", "name");
            ViewBag.id_products = new SelectList(db.products, "id_products", "product_id");
            return View();
        }

        //
        // POST: /Order/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(product_order product_order)
        {
            if (ModelState.IsValid)
            {
                db.product_order.Add(product_order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_manufacturers = new SelectList(db.manufacturers, "id_manufacturers", "name", product_order.id_manufacturers);
            ViewBag.id_products = new SelectList(db.products, "id_products", "product_id", product_order.id_products);
            return View(product_order);
        }

        //
        // GET: /Order/Edit/5

        public ActionResult Edit(int id = 0)
        {
            product_order product_order = db.product_order.Find(id);
            if (product_order == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_manufacturers = new SelectList(db.manufacturers, "id_manufacturers", "name", product_order.id_manufacturers);
            ViewBag.id_products = new SelectList(db.products, "id_products", "product_id", product_order.id_products);
            return View(product_order);
        }

        //
        // POST: /Order/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(product_order product_order)
        {


            if (ModelState.IsValid)
            {
                db.Entry(product_order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_manufacturers = new SelectList(db.manufacturers, "id_manufacturers", "name", product_order.id_manufacturers);
            ViewBag.id_products = new SelectList(db.products, "id_products", "product_id", product_order.id_products);
            return View(product_order);
        }

        //
        // GET: /Order/Delete/5

        public ActionResult Delete(int id = 0)
        {
            product_order product_order = db.product_order.Find(id);
            //if (product == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(product);
            ViewBag.Deletethis = product_order.product.name + " order record";
            return PartialView("_Confirmation");
        }

        //
        // POST: /Order/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {   
            try
            {
                product_order product_order = db.product_order.Find(id);
                db.product_order.Remove(product_order);
                db.SaveChanges();
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false });
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}