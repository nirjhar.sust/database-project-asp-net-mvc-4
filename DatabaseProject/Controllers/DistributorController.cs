﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatabaseProject.Controllers
{
    public class DistributorController : Controller
    {
        private ScerpEntities db = new ScerpEntities();

        //
        // GET: /Distributor/

        public ActionResult Index()
        {
            var distributors = db.distributors.Include(d => d.person_info);
            return View(distributors.ToList());
        }

        //
        // GET: /Distributor/Details/5

        public ActionResult Details(int id = 0)
        {
            distributor distributor = db.distributors.Find(id);
            if (distributor == null)
            {
                return HttpNotFound();
            }
            return View(distributor);
        }

        //
        // GET: /Distributor/Create

        public ActionResult Create()
        {
            ViewBag.id_contact_person = new SelectList(db.person_info, "id_person", "name");
            return View();
        }

        //
        // POST: /Distributor/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(distributor distributor,int temp)
        {
            if (temp == 1)
            {
                if (ModelState.IsValid)
                {
                    string samecontact = Request["optionsRadios"];
                    if (samecontact == "3")
                    {
                        string newcontactname = Request["newcontactname"];
                        string newcontactdescription = Request["newcontactdescription"];
                        string newcontactaddress = Request["newcontactaddress"];
                        string newcontactcontact = Request["newcontactcontact"];

                        var empQuery = from emp in db.person_info
                                       select emp;
                        List<DatabaseProject.person_info> empList = empQuery.ToList();


                        person_info person = new person_info();
                        person.id_person = empList.Count + 2;
                        person.name = newcontactname;
                        person.description = newcontactdescription;
                        person.address = newcontactaddress;
                        person.contact_number = newcontactcontact;
                        person.id_type = 2;

                        db.person_info.Add(person);
                        distributor.id_contact_person = person.id_person;
                    }
                    db.distributors.Add(distributor);
                    db.SaveChanges();
                    //return RedirectToAction("Index");
                    return Content("Record added.<script>var url = '/distributor/';window.location.href = url;</script>");
                }
            }
            else if (temp == 2)
            {
                try
                {
                    string samecontact = Request["optionsRadios"];
                    if (samecontact == "3")
                    {
                        string newcontactname = Request["newcontactname"];
                        string newcontactdescription = Request["newcontactdescription"];
                        string newcontactaddress = Request["newcontactaddress"];
                        string newcontactcontact = Request["newcontactcontact"];

                        var empQuery = from emp in db.person_info
                                       select emp;
                        List<DatabaseProject.person_info> empList = empQuery.ToList();


                        person_info person = new person_info();
                        person.id_person = empList.Count + 2;
                        person.name = newcontactname;
                        person.description = newcontactdescription;
                        person.address = newcontactaddress;
                        person.contact_number = newcontactcontact;
                        person.id_type = 2;

                        db.person_info.Add(person);
                        distributor.id_contact_person = person.id_person;
                    }
                    db.distributors.Add(distributor);
                    db.SaveChanges();
                    return Json(new { success = true });
                }
                catch (Exception e)
                {
                    return Json(new { success = false });
                }
            }

            ViewBag.id_contact_person = new SelectList(db.person_info, "id_person", "name", distributor.id_contact_person);
            return View(distributor);
        }

        //
        // GET: /Distributor/Edit/5

        public ActionResult Edit(int id = 0)
        {
            distributor distributor = db.distributors.Find(id);
            if (distributor == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_contact_person = new SelectList(db.person_info, "id_person", "name", distributor.id_contact_person);
            return View(distributor);
        }

        //
        // POST: /Distributor/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(distributor distributor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(distributor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_contact_person = new SelectList(db.person_info, "id_person", "name", distributor.id_contact_person);
            return View(distributor);
        }

        //
        // GET: /Distributor/Delete/5

        public ActionResult Delete(int id = 0)
        {
            distributor distributor = db.distributors.Find(id);
            //if (distributor == null)
            //{
            //    return HttpNotFound();
            //}
            ViewBag.Deletethis = distributor.name;
            return PartialView("_ConfirmWareHouseDelete");
        }

        //
        // POST: /Distributor/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                distributor distributor = db.distributors.Find(id);
                db.distributors.Remove(distributor);
                db.SaveChanges();
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false });
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}