﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatabaseProject.Controllers
{
    public class DistributionController : Controller
    {
        private ScerpEntities db = new ScerpEntities();


        public ActionResult CreateDistributor()
        {
            //return View("~/Views/Product/Create.cshtml");
            return PartialView("_CreateDistributors");
        }

        //
        // GET: /Distribution/

        public ActionResult Index()
        {
            var distributions = db.distributions.Include(d => d.distributor).Include(d => d.stock);
            return View(distributions.ToList());
        }

        //
        // GET: /Distribution/Details/5

        public ActionResult Details(int id = 0)
        {
            distribution distribution = db.distributions.Find(id);
            if (distribution == null)
            {
                return HttpNotFound();
            }
            return View(distribution);
        }

        //
        // GET: /Distribution/Create

        public ActionResult Create()
        {
            ViewBag.id_distributors = new SelectList(db.distributors, "id_distributors", "name");
            ViewBag.id_stocks = new SelectList(db.stocks, "id_stocks", "name");
            return View();
        }

        //
        // POST: /Distribution/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(distribution distribution)
        {
            if (ModelState.IsValid)
            {
                db.distributions.Add(distribution);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_distributors = new SelectList(db.distributors, "id_distributors", "name", distribution.id_distributors);
            ViewBag.id_stocks = new SelectList(db.stocks, "id_stocks", "name", distribution.id_stocks);
            return View(distribution);
        }

        //
        // GET: /Distribution/Edit/5

        public ActionResult Edit(int id = 0)
        {
            distribution distribution = db.distributions.Find(id);
            if (distribution == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_distributors = new SelectList(db.distributors, "id_distributors", "name", distribution.id_distributors);
            ViewBag.id_stocks = new SelectList(db.stocks, "id_stocks", "name", distribution.id_stocks);
            return View(distribution);
        }

        //
        // POST: /Distribution/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(distribution distribution)
        {
            if (ModelState.IsValid)
            {
                db.Entry(distribution).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_distributors = new SelectList(db.distributors, "id_distributors", "name", distribution.id_distributors);
            ViewBag.id_stocks = new SelectList(db.stocks, "id_stocks", "name", distribution.id_stocks);
            return View(distribution);
        }

        //
        // GET: /Distribution/Delete/5

        public ActionResult Delete(int id = 0)
        {
            distribution distribution = db.distributions.Find(id);
            if (distribution == null)
            {
                return HttpNotFound();
            }
            return View(distribution);
        }

        //
        // POST: /Distribution/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            distribution distribution = db.distributions.Find(id);
            db.distributions.Remove(distribution);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}