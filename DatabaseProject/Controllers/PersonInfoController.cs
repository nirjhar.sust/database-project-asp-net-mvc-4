﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatabaseProject.Controllers
{
    public class PersonInfoController : Controller
    {
        private ScerpEntities db = new ScerpEntities();

        //
        // GET: /PersonInfo/

        public ActionResult Index()
        {
            var person_info = db.person_info.Include(p => p.person_type);
            return View(person_info.ToList());
        }

        //
        // GET: /PersonInfo/Details/5

        public ActionResult Details(int id = 0)
        {
            person_info person_info = db.person_info.Find(id);
            if (person_info == null)
            {
                return HttpNotFound();
            }
            return View(person_info);
        }

        //
        // GET: /PersonInfo/Create

        public ActionResult Create()
        {
            ViewBag.id_type = new SelectList(db.person_type, "id_type", "name");
            return View();
        }

        //
        // POST: /PersonInfo/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(person_info person_info, int temp)
        {

            if (temp == 1)
            {
                person_info.id_type = 1;
                if (ModelState.IsValid)
                {
                    db.person_info.Add(person_info);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else if (temp == 2)
            {
                try
                {
                    db.person_info.Add(person_info);
                    db.SaveChanges();
                    return Json(new { success = true });
                }
                catch (Exception e)
                {
                    return Json(new { success = false });
                }
            }

            ViewBag.id_type = new SelectList(db.person_type, "id_type", "name", person_info.id_type);
            return View(person_info);
        }

        //
        // GET: /PersonInfo/Edit/5

        public ActionResult Edit(int id = 0)
        {
            person_info person_info = db.person_info.Find(id);
            if (person_info == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_type = new SelectList(db.person_type, "id_type", "name", person_info.id_type);
            return View(person_info);
        }

        //
        // POST: /PersonInfo/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(person_info person_info)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person_info).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_type = new SelectList(db.person_type, "id_type", "name", person_info.id_type);
            return View(person_info);
        }

        //
        // GET: /PersonInfo/Delete/5

        public ActionResult Delete(int id = 0)
        {
            person_info person_info = db.person_info.Find(id);

            ViewBag.Deletethis = person_info.name;
            return PartialView("_Confirmation");
        }

        //
        // POST: /PersonInfo/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                person_info person_info = db.person_info.Find(id);
                db.person_info.Remove(person_info);
                db.SaveChanges();
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false });
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}