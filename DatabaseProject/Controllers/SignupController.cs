﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatabaseProject.Controllers
{
    public class SignupController : Controller
    {

        private ScerpEntities db = new ScerpEntities();
        //
        // GET: /Signup/

        public RedirectToRouteResult RedNow()
        {
            return RedirectToAction("Index", "Product");
        }

        public ActionResult Index()
        {
            if (Request["txtname"] ==null)
            {
                return View();
            }
            else
            {
                string txtemail = Request["txtemail"].ToString();
                string txtname = Request["txtname"].ToString();
                string txtpass = Request["txtpass"].ToString();
                string txtAddress = Request["txtAddress"].ToString();
                string txtContact = Request["txtContact"].ToString();

                if (txtemail == "" || txtname == "" || txtpass == "")
                {
                    //return Content("Even I have a name and email.<script>alert(\"some message\");var url = '/Product/Index/';window.location.href = url;</script>");
                    return Content("Even I have a name and email !");
                }


                var EmailQuery = from emp in db.user_panel where emp.email.Equals(txtemail) select emp;
                List<user_panel> emaillist = EmailQuery.ToList();
                if (emaillist.Count == 0)
                {
                    person_type type = new person_type();
                    type.id_type = 0;
                    type.name = "SysAdmin";
                    type.description = "System Administrator";

                    var empQuery = from emp in db.person_type
                                   where emp.name.Equals("SysAdmin")
                                   select emp;
                    int index1 = 0;
                    try
                    {
                        List<person_type> typelist = empQuery.ToList();
                        if (typelist.Count == 0)
                        {
                            db.person_type.Add(type);
                            db.SaveChanges();
                            index1 = type.id_type;
                        }
                        else if (typelist.Count > 0)
                        {
                            index1 = typelist[0].id_type;
                        }

                        person_info info = new person_info();
                        info.id_person = 0;
                        info.name = txtname;
                        info.description = "";
                        info.address = txtAddress;
                        info.contact_number = txtContact;
                        info.id_type = index1;

                        db.person_info.Add(info);
                        db.SaveChanges();
                        int index2 = info.id_person;

                        user_panel userpanel = new user_panel();
                        userpanel.id_user = 0;
                        userpanel.email = txtemail;
                        userpanel.password = txtpass;
                        userpanel.id_person = index2;

                        db.user_panel.Add(userpanel);
                        db.SaveChanges();

                        //alert(\"some \");
                        return Content(txtname + " added.<script>var url = '/Product/';window.location.href = url;</script>");
                    }
                    catch
                    {
                        return Content("Something went wrong! Sending monkeys to fix.");
                    }
                }
                else
                {
                    return Content("Email already exists");
                    //return Content("<script>alert(\"some message\");windows.location.href=\"/Product/Index/\";</script>");
                }
                
            }
        }

    }
}
