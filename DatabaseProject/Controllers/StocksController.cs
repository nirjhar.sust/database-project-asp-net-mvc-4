﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatabaseProject.Controllers
{
    public class StocksController : Controller
    {
        private ScerpEntities db = new ScerpEntities();

        //
        // GET: /Stocks/

        public ActionResult Index()
        {
            var stocks = db.stocks.Include(s => s.product).Include(s => s.warehouse);
            return View(stocks.ToList());
        }

        //
        // GET: /Stocks/Details/5

        public ActionResult Details(int id = 0)
        {
            stock stock = db.stocks.Find(id);
            if (stock == null)
            {
                return HttpNotFound();
            }
            return View(stock);
        }

        //
        // GET: /Stocks/Create

        public ActionResult Create()
        {
            ViewBag.id_products = new SelectList(db.products, "id_products", "product_id");
            ViewBag.id_warehouses = new SelectList(db.warehouses, "id_warehouses", "name");
            return View();
        }

        //
        // POST: /Stocks/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(stock stock)
        {
            if (ModelState.IsValid)
            {
                db.stocks.Add(stock);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_products = new SelectList(db.products, "id_products", "product_id", stock.id_products);
            ViewBag.id_warehouses = new SelectList(db.warehouses, "id_warehouses", "name", stock.id_warehouses);
            return View(stock);
        }

        //
        // GET: /Stocks/Edit/5

        public ActionResult Edit(int id = 0)
        {
            stock stock = db.stocks.Find(id);
            if (stock == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_products = new SelectList(db.products, "id_products", "product_id", stock.id_products);
            ViewBag.id_warehouses = new SelectList(db.warehouses, "id_warehouses", "name", stock.id_warehouses);
            return View(stock);
        }

        //
        // POST: /Stocks/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(stock stock)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stock).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_products = new SelectList(db.products, "id_products", "product_id", stock.id_products);
            ViewBag.id_warehouses = new SelectList(db.warehouses, "id_warehouses", "name", stock.id_warehouses);
            return View(stock);
        }

        //
        // GET: /Stocks/Delete/5

        public ActionResult Delete(int id = 0)
        {
            stock stock = db.stocks.Find(id);
            //if (stock == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(stock);
            ViewBag.Deletethis = stock.name;
            return PartialView("_ConfirmStockDelete");
        }

        //
        // POST: /Stocks/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                stock stock = db.stocks.Find(id);
                db.stocks.Remove(stock);
                db.SaveChanges();
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false });
            }
        }

        public ActionResult CreateWarehouse()
        {
            //return View("~/Views/Product/Create.cshtml");
            return PartialView("_CreateWarehouse");
        }

        public ActionResult CreateProduct()
        {
            //return View("~/Views/Product/Create.cshtml");
            return PartialView("_Createproduct");
        }

        public ActionResult CreatePerson()
        {
            //return View("~/Views/Product/Create.cshtml");
            return PartialView("_CreatePerson");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}