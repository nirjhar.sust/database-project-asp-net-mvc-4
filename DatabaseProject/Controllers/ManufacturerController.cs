﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatabaseProject.Controllers
{
    public class ManufacturerController : Controller
    {
        private ScerpEntities db = new ScerpEntities();

        //
        // GET: /Manufacturer/

        public ActionResult Index()
        {
            var manufacturers = db.manufacturers.Include(m => m.location).Include(m => m.person_info).Include(m => m.person_info1);
            return View(manufacturers.ToList());
        }

        //
        // GET: /Manufacturer/Details/5

        public ActionResult Details(int id = 0)
        {
            manufacturer manufacturer = db.manufacturers.Find(id);
            if (manufacturer == null)
            {
                return HttpNotFound();
            }
            return View(manufacturer);
        }

        //
        // GET: /Manufacturer/Create

        public ActionResult Create()
        {
            ViewBag.id_location = new SelectList(db.locations, "id_location", "name");
            ViewBag.id_owner = new SelectList(db.person_info, "id_person", "name");
            ViewBag.id_contact_person = new SelectList(db.person_info, "id_person", "name");
            return View();
        }

        //
        // POST: /Manufacturer/Create

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(manufacturer manufacturer,int temp)
        {
            if (temp == 1)
            {
                if (true)//ModelState.IsValid)
                {
                    string newLocationName = Request["newLocationName"];
                    string newLocationCity = Request["newLocationCity"];
                    string newLocationDistrict = Request["newLocationDistrict"];
                    string createnewlocation = Request["checkcreatenew"];
                    string createnewowner = Request["createnewowner"];
                    string samecontact = Request["optionsRadios"];
                    if (createnewlocation == "true")
                    {
                        var empQuery = from emp in db.locations
                                       where emp.name == newLocationName
                                       select emp;
                        List<DatabaseProject.location> empList = empQuery.ToList();
                        if (empList.Count == 0)
                        {
                            try
                            {
                                location loc = new location();
                                loc.name = newLocationName;
                                loc.city = newLocationCity;
                                loc.district = newLocationDistrict;
                                db.locations.Add(loc);

                                manufacturer.id_location = loc.id_location;
                            }
                            catch
                            {
                                return Content(newLocationName + " cannot be saved.<script>var url = '/manufacturer/';window.location.href = url;</script>");
                            }
                        }
                        else if (empList.Count > 0)
                        {
                            manufacturer.id_location = empList[0].id_location;
                        }
                    }
                    if (createnewowner == "true")
                    {
                        string newownername = Request["newownername"];
                        string newownerdescription = Request["newownerdescription"];
                        string newowneraddress = Request["newowneraddress"];
                        string newownercontact = Request["newownercontact"];

                        person_info person = new person_info();
                        person.name = newownername;
                        person.description = newownerdescription;
                        person.address = newowneraddress;
                        person.contact_number = newownercontact;

                        var empQuery = from emp in db.person_info
                                       select emp;
                        List<DatabaseProject.person_info> empList = empQuery.ToList();
                        person.id_person = empList.Count + 1;
                        person.id_type = 1;
                        if (samecontact == "2")
                        {
                            person.id_type = 3;
                            manufacturer.id_contact_person = person.id_person;
                        }
                        db.person_info.Add(person);
                        manufacturer.id_owner = person.id_person;
                    }
                    if (samecontact == "3")
                    {
                        string newcontactname = Request["newcontactname"];
                        string newcontactdescription = Request["newcontactdescription"];
                        string newcontactaddress = Request["newcontactaddress"];
                        string newcontactcontact = Request["newcontactcontact"];

                        person_info person = new person_info();
                        person.name = newcontactname;
                        person.description = newcontactdescription;
                        person.address = newcontactaddress;
                        person.contact_number = newcontactcontact;

                        var empQuery = from emp in db.person_info
                                       select emp;
                        List<DatabaseProject.person_info> empList = empQuery.ToList();
                        if (createnewowner == "true")
                        {
                            person.id_person = empList.Count + 2;
                        }
                        else
                        {
                            person.id_person = empList.Count + 1;
                        }

                        person.id_type = 2;

                        db.person_info.Add(person);
                        manufacturer.id_contact_person = person.id_person;
                    }

                    db.manufacturers.Add(manufacturer);
                    db.SaveChanges();
                    return Content(newLocationCity + " added.<script>var url = '/manufacturer/';window.location.href = url;</script>");
                }


            }
            else if (temp == 2)
            {
                try
                {
                    db.manufacturers.Add(manufacturer);
                    db.SaveChanges();
                    return Json(new { success = true });
                }
                catch (Exception e)
                {
                    return Json(new { success = false });
                }
            }
            //if (ModelState.IsValid)
            //{
            //    db.manufacturers.Add(manufacturer);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            ViewBag.id_location = new SelectList(db.locations, "id_location", "name", manufacturer.id_location);
            ViewBag.id_owner = new SelectList(db.person_info, "id_person", "name", manufacturer.id_owner);
            ViewBag.id_contact_person = new SelectList(db.person_info, "id_person", "name", manufacturer.id_contact_person);
            return View(manufacturer);
        }

        //
        // GET: /Manufacturer/Edit/5

        public ActionResult Edit(int id = 0)
        {
            manufacturer manufacturer = db.manufacturers.Find(id);
            if (manufacturer == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_location = new SelectList(db.locations, "id_location", "name", manufacturer.id_location);
            ViewBag.id_owner = new SelectList(db.person_info, "id_person", "name", manufacturer.id_owner);
            ViewBag.id_contact_person = new SelectList(db.person_info, "id_person", "name", manufacturer.id_contact_person);
            return View(manufacturer);
        }

        //
        // POST: /Manufacturer/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(manufacturer manufacturer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(manufacturer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_location = new SelectList(db.locations, "id_location", "name", manufacturer.id_location);
            ViewBag.id_owner = new SelectList(db.person_info, "id_person", "name", manufacturer.id_owner);
            ViewBag.id_contact_person = new SelectList(db.person_info, "id_person", "name", manufacturer.id_contact_person);
            return View(manufacturer);
        }

        //
        // GET: /Manufacturer/Delete/5

        public ActionResult Delete(int id = 0)
        {
            manufacturer manufacturer = db.manufacturers.Find(id);
            //if (manufacturer == null)
            //{
            //    return HttpNotFound();
            //}
            ViewBag.Deletethis = manufacturer.name;
            return PartialView("_Confirmation");
        }

        //
        // POST: /Manufacturer/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                manufacturer manufacturer = db.manufacturers.Find(id);
                db.manufacturers.Remove(manufacturer);
                db.SaveChanges();
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { success = false });
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}